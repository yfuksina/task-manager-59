package ru.tsc.fuksina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.ServerAboutRequest;
import ru.tsc.fuksina.tm.event.ConsoleEvent;

@Component
public final class AboutListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Display developer info";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@aboutListener.getName() == #event.name || @aboutListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        @NotNull final ServerAboutRequest request = new ServerAboutRequest();
        System.out.println("Author: " + getPropertyService().getAuthorName());
        System.out.println("Email: " + getPropertyService().getAuthorEmail());
    }

}
