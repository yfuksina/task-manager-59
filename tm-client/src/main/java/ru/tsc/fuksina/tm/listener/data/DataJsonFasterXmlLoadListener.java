package ru.tsc.fuksina.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.dto.request.DataJsonFasterXmlLoadRequest;
import ru.tsc.fuksina.tm.dto.request.UserLogoutRequest;
import ru.tsc.fuksina.tm.enumerated.Role;
import ru.tsc.fuksina.tm.event.ConsoleEvent;

@Component
public final class DataJsonFasterXmlLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-json-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data in json file";
    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonFasterXmlLoadListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SAVE DATA IN JSON FILE]");
        getDomainEndpoint().loadDataJsonFasterXml(new DataJsonFasterXmlLoadRequest(getToken()));
        getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

}
