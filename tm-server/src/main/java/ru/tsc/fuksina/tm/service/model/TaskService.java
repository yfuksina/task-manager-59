package ru.tsc.fuksina.tm.service.model;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.fuksina.tm.api.repository.model.ITaskRepository;
import ru.tsc.fuksina.tm.api.service.model.ITaskService;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.exception.field.*;
import ru.tsc.fuksina.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Getter
@Service
public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        @NotNull final Task task = new Task();
        task.setUser(getUserRepository().findOneById(userId));
        task.setName(name);
        @NotNull final ITaskRepository repository = getRepository();
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Task task = new Task();
        task.setUser(getUserRepository().findOneById(userId));
        task.setName(name);
        task.setDescription(description);
        @NotNull final ITaskRepository repository = getRepository();
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateStart,
            @Nullable final Date dateEnd
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Task task = new Task();
        task.setUser(getUserRepository().findOneById(userId));
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(dateStart);
        task.setDateEnd(dateEnd);
        @NotNull final ITaskRepository repository = getRepository();
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).filter(item -> !item.isEmpty()).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).filter(item -> !item.isEmpty()).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        @NotNull final ITaskRepository repository = getRepository();
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).filter(item -> !item.isEmpty()).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusEmptyException::new);
        @NotNull final Task task = findOneById(userId, id);
        task.setStatus(status);
        @NotNull final ITaskRepository repository = getRepository();
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).filter(item -> !item.isEmpty()).orElseThrow(UserIdEmptyException::new);
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final ITaskRepository repository = getRepository();
        return repository.findAllByProjectId(userId, projectId);
    }

}
